/* 
 * File:   marco_clock.c
 * Author: fsedano
 *
 * Created on January 19, 2013, 12:50 AM
 */


#include "p18f25k80.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <i2c.h>

#pragma config         FOSC = HS2, PWRTEN = OFF
#pragma config         PLLCFG = ON
#pragma config         BOREN = OFF ,WDTEN = OFF , MCLRE = ON
#pragma config         STVREN = ON  ,XINST = OFF , SOSCSEL = DIG
#pragma config			CANMX = PORTC

#define TOGGLE_DEBUG            { static int i=0; if (i) { \
                                LATCbits.LATC7 = 1; \
                                i = 0; \
                                } else { \
                                LATCbits.LATC7 = 0; \
                                i = 1; \
                                }}


#define BEEP                            LATCbits.LATC1
//#define A_74164                         LATCbits.LATC3
#define A_74164                         LATCbits.LATC7
#define CLK_74164                       LATCbits.LATC2
#define CLK                             LATCbits.LATC5
//#define DAT                             LATCbits.LATC4
#define DAT                             LATBbits.LATB0
#define RADIO                           PORTCbits.RC6

#define PB0                             PORTBbits.RB1
#define PB1                             PORTBbits.RB2
#define PB2                             PORTBbits.RB3
#define PB3                             PORTBbits.RB4
#define PB4                             PORTBbits.RB5

#define     EEPROM_HOUR 0
#define     EEPROM_MIN  1
#define     EEPROM_DOW  2
#define     EEPROM_HOUR_BED 3
#define     EEPROM_HOUR_WAKE 4
#define     EEPROM_HOUR_BED_WE 3
#define     EEPROM_HOUR_WAKE_WE 4


#define SLAVE_MODE                      0b100000100000
#define RC_MASTER_MODE                  0b100000110000 //[HT1632C]Set master mode and clock source from on-chip RC oscillator
#define EXT_CLK_MASTER_MODE             0b100000111000 //[HT1632C]Set master mode and clock source from external clock

#define SYS_EN                          0b100000000010
#define LED_ON                          0b100000000110

#define N_MOS_COM8                      0b100001000000


#define TIMER_SHOW                      50

#define PWM_10                          0b100101010010
#define PWM_02                          0b100101000010
#define BLK_OFF                         0b100000010000
#define BLK_ON                          0b100000010010

#define CHIP_MAX        4

/*
 * 
 */


struct _ti {
    unsigned int
        Second,
            Minute,
            Hour,
            Day,
            Month,
            Year,
            CEST,
            valid;

} tRadio;

typedef struct _s77 {
  unsigned int summer:1;
  unsigned int cest:1;
  unsigned int cet:1;
  unsigned int min:7;
  unsigned int P1:1;
  unsigned int hour:6;
  unsigned int P2:1;
  unsigned int day:6;
  unsigned int dow:3;
  unsigned int month:5;
  unsigned int year:8;
  unsigned int P3:1;
  unsigned int calc_P1:1;
  unsigned int calc_P2:1;
  unsigned int calc_P3:1;
  unsigned int valid:1;
} DCF77;

unsigned char powered_off;
unsigned char debounce_reg[5];
unsigned char last_button_state[5];
typedef enum  {
    CONFIG_NORMAL,
    CONFIG_HOUR,
    CONFIG_MIN,
    CONFIG_DOW,
    CONFIG_HOUR_BED,
    CONFIG_HOUR_WAKE,
    CONFIG_HOUR_BED_WE,
    CONFIG_HOUR_WAKE_WE,
    CONFIG_MAX} _config_state;
static _config_state config_state = CONFIG_NORMAL;
volatile int hour, minute, second,dow,hour_bed, hour_wake, hour_bed_we, hour_wake_we;

unsigned char t_data[256];
volatile unsigned char clock_buf[256];
volatile unsigned char clock_ready;
//unsigned int t_data_w[256];

unsigned char t_idx;

void SetHT1632C_As3208(void);
void OutputCLK_Pulse(void);
void OutputA_74164(unsigned char x);
void ChipSelect(int select);

void CommandWriteHT1632C(unsigned int command); //Write command to ALL HT1632Cs
void AddressWriteHT1632C(unsigned char address);//Write Address to HT1632Cs
void WriteHT1632C(unsigned char chip, unsigned char myaddress, unsigned char data);
void SPI_ModelConfigure(void);
void SPI_DataSend(const unsigned char data);
#if 0
        char data[] = {
            0,0,0,
            0b00000011,
            0b00000100,
            0b00000010,
            0b00000010,
            0b00000010,
            0b00000010,
            0b00000010,
            0b00000100,
            0b00000100,
            0b00000011,
            0,0,0,0,0,0,0,0,
            0b01000000,
            0b11111100,
            0,
            0b00000001,
            0b11111101,
            0b11111101,
            0b11111101,
            0b11111100,
            0b11111100,
            0b11111100 };

        char data2[] = {
            0,0,0,0,
            0b11110000,
            0b00001000,
            0b00100100,
            0b10100010,
            0b00011010,
            0b10100010,
            0b00100100,
            0b00001000,
            0b11110000,
            0,0,0,0,0,0,0,0,0,0,
            0b01111000,
            0b11111100,
            0b11111110,
            0b11111110,
            0b11111110,
            0b11111110,
            0b11111100,
            0b01111000,
            0 };
#endif
        char balls[][8] = {
            {
             0b00111100,
             0b01000010,
             0b10000001,
             0b10000001,
             0b10000001,
             0b10000001,
             0b01000010,
             0b00111100
            },
            {
             0b00111100,
             0b01000010,
             0b10000001,
             0b10000001,
             0b11110001,
             0b11110001,
             0b01110010,
             0b00111100,
            },
            {
             0b00111100,
             0b01000010,
             0b10000001,
             0b10000001,
             0b11111111,
             0b11111111,
             0b01111110,
             0b00111100,
            },
            {
             0b00111100,
             0b01001110,
             0b10001111,
             0b10001111,
             0b11111111,
             0b11111111,
             0b01111110,
             0b00111100,
            },

            {
             0b00111100,
             0b01111110,
             0b11111111,
             0b11111111,
             0b11111111,
             0b11111111,
             0b01111110,
             0b00111100,
            },

        };

        char bear_0_awake[] = {
            0b00111000,
            0b01000100,
            0b01010011,
            0b01000000, //5
            0b00100000,
            0b00010011,
            0b00010011,
            0b00010000,
            0b00010000, //10
            0b00010000,
            0b00010011,
            0b00010011,
            0b00100000,
            0b01000000, //15
            0b01010011,
            0b01000100,
            0b00111000,
            0,          // 19
        };
        char bear_1_awake[] = {
            0,0,
            0b11111000,
            0b00000100, // 5
            0b00000010,
            0b00000001,
            0b01001001,
            0b01100101,
            0b01111101, //10
            0b01100101,
            0b01001001,
            0b00000001,
            0b00000010,
            0b00000100, //15
            0b11111000,
            0,0,0,      //19
        };

        char bear_0a_awake[] = {
            0b00111000,
            0b01000100,
            0b01010011,
            0b01000000,
            0b00100000,//5
            0b00010011,
            0b00010011,
            0b00010000,
            0b00010000,
            0b00010000,//10
            0b00010011,
            0b00010011,
            0b00100000,
            0b01000000, //15
            0b01010011,
            0b01000100,
            0b00111000,
            0,          // 19
        };
        char bear_1a_awake[] = {
            0,0,
            0b11111000,
            0b00000100, // 4
            0b00000010,
            0b00000001,
            0b01000101,
            0b01100101,
            0b01111101, //10
            0b01100101,
            0b01000101,
            0b00000001,
            0b00000010,
            0b00000100, //15
            0b11111000,
            0,0,0,      //19
        };

        char bear_0_slept[] = {
            0b00111000,
            0b01000100,
            0b01010011,
            0b01000000, //5
            0b00100001,
            0b00010010,
            0b00010010,
            0b00010000,
            0b00010000, //10
            0b00010000,
            0b00010010,
            0b00010010,
            0b00100001,
            0b01000000, //15
            0b01010011,
            0b01000100,
            0b00111000,
            0,          // 19
        };
        char bear_1_slept[] = {
            0,0,
            0b11111000,
            0b00000100, // 5
            0b00000010,
            0b00000001,
            0b01001001,
            0b01100101,
            0b01111101, //10
            0b01100101,
            0b01001001,
            0b00000001,
            0b00000010,
            0b00000100, //15
            0b11111000,
            0,0,0,      //19
        };

        char bear_0a_slept[] = {
            0b00111000,
            0b01000100,
            0b01010011,
            0b01000000, //5
            0b00100010,
            0b00010010,
            0b00010010,
            0b00010000,
            0b00010000, //10
            0b00010000,
            0b00010010,
            0b00010010,
            0b00100010,
            0b01000000, //15
            0b01010011,
            0b01000100,
            0b00111000,
            0,          // 19
        };
        char bear_1a_slept[] = {
            0,0,
            0b11111000,
            0b00000100, // 5
            0b00000010,
            0b00000001,
            0b01001001,
            0b01100101,
            0b01111101, //10
            0b01100101,
            0b01001001,
            0b00000001,
            0b00000010,
            0b00000100, //15
            0b11111000,
            0,0,0,      //19
        };
        

const unsigned char font[] =
{
        0x3C, 0x66, 0x6E, 0x6E, 0x60, 0x62, 0x3C, 0x00, // Char 000 (.)
        0x00, 0x00, 0x3C, 0x06, 0x3E, 0x66, 0x3E, 0x00, // Char 001 (.)
        0x00, 0x60, 0x60, 0x7C, 0x66, 0x66, 0x7C, 0x00, // Char 002 (.)
        0x00, 0x00, 0x3C, 0x60, 0x60, 0x60, 0x3C, 0x00, // Char 003 (.)
        0x00, 0x06, 0x06, 0x3E, 0x66, 0x66, 0x3E, 0x00, // Char 004 (.)
        0x00, 0x00, 0x3C, 0x66, 0x7E, 0x60, 0x3C, 0x00, // Char 005 (.)
        0x00, 0x0E, 0x18, 0x3E, 0x18, 0x18, 0x18, 0x00, // Char 006 (.)
        0x00, 0x00, 0x3E, 0x66, 0x66, 0x3E, 0x06, 0x7C, // Char 007 (.)
        0x00, 0x60, 0x60, 0x7C, 0x66, 0x66, 0x66, 0x00, // Char 008 (.)
        0x00, 0x18, 0x00, 0x38, 0x18, 0x18, 0x3C, 0x00, // Char 009 (.)
        0x00, 0x06, 0x00, 0x06, 0x06, 0x06, 0x06, 0x3C, // Char 010 (.)
        0x00, 0x60, 0x60, 0x6C, 0x78, 0x6C, 0x66, 0x00, // Char 011 (.)
        0x00, 0x38, 0x18, 0x18, 0x18, 0x18, 0x3C, 0x00, // Char 012 (.)
        0x00, 0x00, 0x66, 0x7F, 0x7F, 0x6B, 0x63, 0x00, // Char 013 (.)
        0x00, 0x00, 0x7C, 0x66, 0x66, 0x66, 0x66, 0x00, // Char 014 (.)
        0x00, 0x00, 0x3C, 0x66, 0x66, 0x66, 0x3C, 0x00, // Char 015 (.)
        0x00, 0x00, 0x7C, 0x66, 0x66, 0x7C, 0x60, 0x60, // Char 016 (.)
        0x00, 0x00, 0x3E, 0x66, 0x66, 0x3E, 0x06, 0x06, // Char 017 (.)
        0x00, 0x00, 0x7C, 0x66, 0x60, 0x60, 0x60, 0x00, // Char 018 (.)
        0x00, 0x00, 0x3E, 0x60, 0x3C, 0x06, 0x7C, 0x00, // Char 019 (.)
        0x00, 0x18, 0x7E, 0x18, 0x18, 0x18, 0x0E, 0x00, // Char 020 (.)
        0x00, 0x00, 0x66, 0x66, 0x66, 0x66, 0x3E, 0x00, // Char 021 (.)
        0x00, 0x00, 0x66, 0x66, 0x66, 0x3C, 0x18, 0x00, // Char 022 (.)
        0x00, 0x00, 0x63, 0x6B, 0x7F, 0x3E, 0x36, 0x00, // Char 023 (.)
        0x00, 0x00, 0x66, 0x3C, 0x18, 0x3C, 0x66, 0x00, // Char 024 (.)
        0x00, 0x00, 0x66, 0x66, 0x66, 0x3E, 0x0C, 0x78, // Char 025 (.)
        0x00, 0x00, 0x7E, 0x0C, 0x18, 0x30, 0x7E, 0x00, // Char 026 (.)
        0x3C, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3C, 0x00, // Char 027 (.)
        0x0C, 0x12, 0x30, 0x7C, 0x30, 0x62, 0xFC, 0x00, // Char 028 (.)
        0x3C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x3C, 0x00, // Char 029 (.)
        0x00, 0x18, 0x3C, 0x7E, 0x18, 0x18, 0x18, 0x18, // Char 030 (.)
        0x00, 0x10, 0x30, 0x7F, 0x7F, 0x30, 0x10, 0x00, // Char 031 (.)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 032 ( )
        0x18, 0x18, 0x18, 0x18, 0x00, 0x00, 0x18, 0x00, // Char 033 (!)
        0x66, 0x66, 0x66, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 034 (")
        0x66, 0x66, 0xFF, 0x66, 0xFF, 0x66, 0x66, 0x00, // Char 035 (#)
        0x18, 0x3E, 0x60, 0x3C, 0x06, 0x7C, 0x18, 0x00, // Char 036 ($)
        0x62, 0x66, 0x0C, 0x18, 0x30, 0x66, 0x46, 0x00, // Char 037 (%)
        0x3C, 0x66, 0x3C, 0x38, 0x67, 0x66, 0x3F, 0x00, // Char 038 (&)
        0x06, 0x0C, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 039 (')
        0x0C, 0x18, 0x30, 0x30, 0x30, 0x18, 0x0C, 0x00, // Char 040 (()
        0x30, 0x18, 0x0C, 0x0C, 0x0C, 0x18, 0x30, 0x00, // Char 041 ())
        0x00, 0x66, 0x3C, 0xFF, 0x3C, 0x66, 0x00, 0x00, // Char 042 (*)
        0x00, 0x18, 0x18, 0x7E, 0x18, 0x18, 0x00, 0x00, // Char 043 (+)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x18, 0x30, // Char 044 (,)
        0x00, 0x00, 0x00, 0x7E, 0x00, 0x00, 0x00, 0x00, // Char 045 (-)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x18, 0x00, // Char 046 (.)
        0x00, 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x00, // Char 047 (/)
        0x3C, 0x66, 0x6E, 0x76, 0x66, 0x66, 0x3C, 0x00, // Char 048 (0)
        0x18, 0x18, 0x38, 0x18, 0x18, 0x18, 0x7E, 0x00, // Char 049 (1)
        0x3C, 0x66, 0x06, 0x0C, 0x30, 0x60, 0x7E, 0x00, // Char 050 (2)
        0x3C, 0x66, 0x06, 0x1C, 0x06, 0x66, 0x3C, 0x00, // Char 051 (3)
        0x06, 0x0E, 0x1E, 0x66, 0x7F, 0x06, 0x06, 0x00, // Char 052 (4)
        0x7E, 0x60, 0x7C, 0x06, 0x06, 0x66, 0x3C, 0x00, // Char 053 (5)
        0x3C, 0x66, 0x60, 0x7C, 0x66, 0x66, 0x3C, 0x00, // Char 054 (6)
        0x7E, 0x66, 0x0C, 0x18, 0x18, 0x18, 0x18, 0x00, // Char 055 (7)
        0x3C, 0x66, 0x66, 0x3C, 0x66, 0x66, 0x3C, 0x00, // Char 056 (8)
        0x3C, 0x66, 0x66, 0x3E, 0x06, 0x66, 0x3C, 0x00, // Char 057 (9)
        0x00, 0x00, 0x18, 0x00, 0x00, 0x18, 0x00, 0x00, // Char 058 (:)
        0x00, 0x00, 0x18, 0x00, 0x00, 0x18, 0x18, 0x30, // Char 059 (;)
        0x0E, 0x18, 0x30, 0x60, 0x30, 0x18, 0x0E, 0x00, // Char 060 (<)
        0x00, 0x00, 0x7E, 0x00, 0x7E, 0x00, 0x00, 0x00, // Char 061 (=)
        0x70, 0x18, 0x0C, 0x06, 0x0C, 0x18, 0x70, 0x00, // Char 062 (>)
        0x3C, 0x66, 0x06, 0x0C, 0x18, 0x00, 0x18, 0x00, // Char 063 (?)
        0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, // Char 064 (@)
        0x18, 0x3C, 0x66, 0x7E, 0x66, 0x66, 0x66, 0x00, // Char 065 (A)
        0x7C, 0x66, 0x66, 0x7C, 0x66, 0x66, 0x7C, 0x00, // Char 066 (B)
        0x3C, 0x66, 0x60, 0x60, 0x60, 0x66, 0x3C, 0x00, // Char 067 (C)
        0x78, 0x6C, 0x66, 0x66, 0x66, 0x6C, 0x78, 0x00, // Char 068 (D)
        0x7E, 0x60, 0x60, 0x78, 0x60, 0x60, 0x7E, 0x00, // Char 069 (E)
        0x7E, 0x60, 0x60, 0x78, 0x60, 0x60, 0x60, 0x00, // Char 070 (F)
        0x3C, 0x66, 0x60, 0x6E, 0x66, 0x66, 0x3C, 0x00, // Char 071 (G)
        0x66, 0x66, 0x66, 0x7E, 0x66, 0x66, 0x66, 0x00, // Char 072 (H)
        0x3C, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3C, 0x00, // Char 073 (I)
        0x1E, 0x0C, 0x0C, 0x0C, 0x0C, 0x6C, 0x38, 0x00, // Char 074 (J)
        0x66, 0x6C, 0x78, 0x70, 0x78, 0x6C, 0x66, 0x00, // Char 075 (K)
        0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x7E, 0x00, // Char 076 (L)
        0x63, 0x77, 0x7F, 0x6B, 0x63, 0x63, 0x63, 0x00, // Char 077 (M)
        0x66, 0x76, 0x7E, 0x7E, 0x6E, 0x66, 0x66, 0x00, // Char 078 (N)
        0x3C, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3C, 0x00, // Char 079 (O)
        0x7C, 0x66, 0x66, 0x7C, 0x60, 0x60, 0x60, 0x00, // Char 080 (P)
        0x3C, 0x66, 0x66, 0x66, 0x66, 0x3C, 0x0E, 0x00, // Char 081 (Q)
        0x7C, 0x66, 0x66, 0x7C, 0x78, 0x6C, 0x66, 0x00, // Char 082 (R)
        0x3C, 0x66, 0x60, 0x3C, 0x06, 0x66, 0x3C, 0x00, // Char 083 (S)
        0x7E, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x00, // Char 084 (T)
        0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3C, 0x00, // Char 085 (U)
        0x66, 0x66, 0x66, 0x66, 0x66, 0x3C, 0x18, 0x00, // Char 086 (V)
        0x63, 0x63, 0x63, 0x6B, 0x7F, 0x77, 0x63, 0x00, // Char 087 (W)
        0x66, 0x66, 0x3C, 0x18, 0x3C, 0x66, 0x66, 0x00, // Char 088 (X)
        0x66, 0x66, 0x66, 0x3C, 0x18, 0x18, 0x18, 0x00, // Char 089 (Y)
        0x7E, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x7E, 0x00, // Char 090 (Z)
        0x18, 0x18, 0x18, 0xFF, 0xFF, 0x18, 0x18, 0x18, // Char 091 ([)
        0xC0, 0xC0, 0x30, 0x30, 0xC0, 0xC0, 0x30, 0x30, // Char 092 (\)
        0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, // Char 093 (])
        0x33, 0x33, 0xCC, 0xCC, 0x33, 0x33, 0xCC, 0xCC, // Char 094 (^)
        0x33, 0x99, 0xCC, 0x66, 0x33, 0x99, 0xCC, 0x66, // Char 095 (_)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 096 (`)
        0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, // Char 097 (a)
        0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, // Char 098 (b)
        0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 099 (c)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, // Char 100 (d)
        0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, // Char 101 (e)
        0xCC, 0xCC, 0x33, 0x33, 0xCC, 0xCC, 0x33, 0x33, // Char 102 (f)
        0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, // Char 103 (g)
        0x00, 0x00, 0x00, 0x00, 0xCC, 0xCC, 0x33, 0x33, // Char 104 (h)
        0xCC, 0x99, 0x33, 0x66, 0xCC, 0x99, 0x33, 0x66, // Char 105 (i)
        0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, // Char 106 (j)
        0x18, 0x18, 0x18, 0x1F, 0x1F, 0x18, 0x18, 0x18, // Char 107 (k)
        0x00, 0x00, 0x00, 0x00, 0x0F, 0x0F, 0x0F, 0x0F, // Char 108 (l)
        0x18, 0x18, 0x18, 0x1F, 0x1F, 0x00, 0x00, 0x00, // Char 109 (m)
        0x00, 0x00, 0x00, 0xF8, 0xF8, 0x18, 0x18, 0x18, // Char 110 (n)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, // Char 111 (o)
        0x00, 0x00, 0x00, 0x1F, 0x1F, 0x18, 0x18, 0x18, // Char 112 (p)
        0x18, 0x18, 0x18, 0xFF, 0xFF, 0x00, 0x00, 0x00, // Char 113 (q)
        0x00, 0x00, 0x00, 0xFF, 0xFF, 0x18, 0x18, 0x18, // Char 114 (r)
        0x18, 0x18, 0x18, 0xF8, 0xF8, 0x18, 0x18, 0x18, // Char 115 (s)
        0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, // Char 116 (t)
        0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, // Char 117 (u)
        0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, // Char 118 (v)
        0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 119 (w)
        0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 119 (w)
        0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, // Char 120 (x)
        0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, // Char 121 (y)
        0x01, 0x03, 0x06, 0x6C, 0x78, 0x70, 0x60, 0x00, // Char 122 (z)
        0x00, 0x00, 0x00, 0x00, 0xF0, 0xF0, 0xF0, 0xF0, // Char 123 ({)
        0x0F, 0x0F, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x00, // Char 124 (|)
        0x18, 0x18, 0x18, 0xF8, 0xF8, 0x00, 0x00, 0x00, // Char 125 (})
        0xF0, 0xF0, 0xF0, 0xF0, 0x00, 0x00, 0x00, 0x00, // Char 126 (~)
        0xF0, 0xF0, 0xF0, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, // Char 127 (.)
        0x00, 0x18, 0x24, 0x2C, 0x34, 0x24, 0x18, 0x00, // Char 048 (0) //128
        0x00, 0x10, 0x30, 0x10, 0x10, 0x10, 0x38, 0x00, // Char 049 (1)
        0x00, 0x18, 0x24, 0x04, 0x18, 0x20, 0x3C, 0x00, // Char 050 (2)
        0x00, 0x38, 0x04, 0x18, 0x04, 0x04, 0x38, 0x00, // Char 051 (3)
        0x00, 0x20, 0x28, 0x28, 0x3C, 0x08, 0x08, 0x00, // Char 052 (4)
        0x00, 0x3C, 0x20, 0x38, 0x04, 0x04, 0x38, 0x00, // Char 053 (5)
        0x00, 0x1C, 0x20, 0x38, 0x24, 0x24, 0x18, 0x00, // Char 054 (6)
        0x00, 0x3C, 0x04, 0x08, 0x10, 0x10, 0x10, 0x00, // Char 055 (7)
        0x00, 0x18, 0x24, 0x18, 0x24, 0x24, 0x18, 0x00, // Char 056 (8)
        0x00, 0x18, 0x24, 0x24, 0x1C, 0x04, 0x38, 0x00, // Char 057 (9)
        0x00, 0x10, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, // Char 058 (:)
        0x00, 0x18, 0x24, 0x24, 0x24, 0x24, 0x18, 0x00, // Char 079 (O) //139
        0x00, 0x3C, 0x20, 0x38, 0x20, 0x20, 0x20, 0x00, // Char 070 (F)
        0x00, 0x24, 0x34, 0x34, 0x2C, 0x2C, 0x24, 0x00, // Char 078 (N)
/////////////////////////////////////////////////////////////////// 142 Tiny fonts
        0x00, 0x18, 0x24, 0x24, 0x3C, 0x24, 0x24, 0x00, // Char 065 (A) 144
        0x00, 0x38, 0x24, 0x38, 0x24, 0x24, 0x38, 0x00, // Char 066 (B) 145
        0x00, 0x18, 0x24, 0x20, 0x20, 0x24, 0x18, 0x00, // Char 067 (C)
        0x00, 0x38, 0x24, 0x24, 0x24, 0x24, 0x38, 0x00, // Char 068 (D) 
        0x00, 0x3C, 0x20, 0x38, 0x20, 0x20, 0x3C, 0x00, // Char 069 (E)
        0x00, 0x3C, 0x20, 0x38, 0x20, 0x20, 0x20, 0x00, // Char 070 (F)
        0x00, 0x18, 0x24, 0x20, 0x2C, 0x24, 0x18, 0x00, // Char 071 (G) 150
        0x00, 0x24, 0x24, 0x3C, 0x24, 0x24, 0x24, 0x00, // Char 072 (H)
        0x00, 0x38, 0x10, 0x10, 0x10, 0x10, 0x38, 0x00, // Char 073 (I) 152
        0x00, 0x04, 0x04, 0x04, 0x04, 0x24, 0x18, 0x00, // Char 074 (J)
        0x00, 0x28, 0x28, 0x30, 0x28, 0x24, 0x24, 0x00, // Char 075 (K)
        0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x3C, 0x00, // Char 076 (L) 155
        0x00, 0x44, 0x6C, 0x54, 0x44, 0x44, 0x44, 0x00, // Char 077 (M)
        0x00, 0x24, 0x34, 0x34, 0x2C, 0x2C, 0x24, 0x00, // Char 078 (N) 157
        0x00, 0x18, 0x24, 0x24, 0x24, 0x24, 0x18, 0x00, // Char 079 (O)
        0x00, 0x38, 0x24, 0x24, 0x38, 0x20, 0x20, 0x00, // Char 080 (P)
        0x00, 0x18, 0x24, 0x24, 0x24, 0x24, 0x1C, 0x02, // Char 081 (Q) 160
        0x00, 0x38, 0x24, 0x24, 0x38, 0x24, 0x24, 0x00, // Char 082 (R)
        0x00, 0x1C, 0x20, 0x18, 0x04, 0x04, 0x38, 0x00, // Char 083 (S) 162
        0x00, 0x38, 0x10, 0x10, 0x10, 0x10, 0x10, 0x00, // Char 084 (T)
        0x00, 0x24, 0x24, 0x24, 0x24, 0x24, 0x3C, 0x00, // Char 085 (U)
        0x00, 0x24, 0x24, 0x24, 0x24, 0x14, 0x0C, 0x00, // Char 086 (V) 165
        0x00, 0x44, 0x44, 0x44, 0x54, 0x6C, 0x44, 0x00, // Char 087 (W)
        0x00, 0x24, 0x24, 0x10, 0x08, 0x24, 0x24, 0x00, // Char 088 (X) 167
        0x00, 0x22, 0x22, 0x14, 0x08, 0x08, 0x08, 0x00, // Char 089 (Y)
        0x00, 0x3C, 0x04, 0x08, 0x10, 0x20, 0x3C, 0x00, // Char 090 (Z)
        0x00, 0x00, 0x18, 0x24, 0x3C, 0x24, 0x24, 0x00, // Char 097 (a) 170
        0x00, 0x00, 0x38, 0x24, 0x38, 0x24, 0x38, 0x00, // Char 098 (b)
        0x00, 0x00, 0x18, 0x24, 0x20, 0x24, 0x18, 0x00, // Char 099 (c) 172
        0x00, 0x00, 0x38, 0x24, 0x24, 0x24, 0x38, 0x00, // Char 100 (d)
        0x00, 0x00, 0x3C, 0x20, 0x38, 0x20, 0x3C, 0x00, // Char 101 (e)
        0x00, 0x00, 0x3C, 0x20, 0x38, 0x20, 0x20, 0x00, // Char 102 (f) 175
        0x00, 0x00, 0x1C, 0x20, 0x2C, 0x24, 0x18, 0x00, // Char 103 (g)
        0x00, 0x00, 0x24, 0x24, 0x3C, 0x24, 0x24, 0x00, // Char 104 (h) 177
        0x00, 0x00, 0x1C, 0x08, 0x08, 0x08, 0x1C, 0x00, // Char 105 (i)
        0x00, 0x00, 0x04, 0x04, 0x04, 0x24, 0x18, 0x00, // Char 106 (j)
        0x00, 0x00, 0x24, 0x28, 0x30, 0x28, 0x24, 0x00, // Char 107 (k) 180
        0x00, 0x00, 0x20, 0x20, 0x20, 0x20, 0x3C, 0x00, // Char 108 (l)
        0x00, 0x00, 0x42, 0x66, 0x5A, 0x42, 0x42, 0x00, // Char 109 (m) 182
        0x00, 0x00, 0x24, 0x34, 0x3C, 0x2C, 0x24, 0x00, // Char 110 (n)
        0x00, 0x00, 0x18, 0x24, 0x24, 0x24, 0x18, 0x00, // Char 111 (o)
        0x00, 0x00, 0x38, 0x24, 0x38, 0x20, 0x20, 0x00, // Char 112 (p) 185
        0x00, 0x00, 0x18, 0x24, 0x24, 0x24, 0x1C, 0x02, // Char 113 (q)
        0x00, 0x00, 0x38, 0x24, 0x38, 0x24, 0x24, 0x00, // Char 114 (r) 187
        0x00, 0x00, 0x1C, 0x20, 0x18, 0x04, 0x38, 0x00, // Char 115 (s)
        0x00, 0x00, 0x38, 0x10, 0x10, 0x10, 0x10, 0x00, // Char 116 (t)
        0x00, 0x00, 0x24, 0x24, 0x24, 0x24, 0x3C, 0x00, // Char 117 (u) 190
        0x00, 0x00, 0x24, 0x24, 0x24, 0x14, 0x0C, 0x00, // Char 118 (v)
        0x00, 0x00, 0x42, 0x42, 0x5A, 0x66, 0x42, 0x00, // Char 119 (w) 192
        0x00, 0x00, 0x44, 0x28, 0x10, 0x28, 0x44, 0x00, // Char 120 (x)
        0x00, 0x00, 0x44, 0x28, 0x10, 0x10, 0x10, 0x00, // Char 121 (y)
        0x00, 0x00, 0x7C, 0x08, 0x10, 0x20, 0x7C, 0x00, // Char 122 (z) 195
        0x00, 0x00, 0x7C, 0x08, 0x10, 0x20, 0x7C, 0x00, // Char 122 (z) 195
};


//////////
// i2c
/////////

unsigned char BCD2Dec(unsigned char x)
{
    unsigned char rc;
    rc = x & 0xF0;
    rc >>= 4;
    rc *= 10;
    rc += (x & 0x0F);
    return rc;
}

unsigned char Dec2BCD(unsigned char x)
{
    // 30 -> 0x30
    unsigned char rc;
    unsigned char d = x/10;
    d <<= 4;
    rc = d;
    rc |= (x%10);
    return rc;

}

void RTC_set()
{
    CloseI2C();
    SSPADD=0x7f;
    OpenI2C(MASTER,SLEW_ON);

    StartI2C();
    IdleI2C();
    WriteI2C( 0xD0 );
    IdleI2C();
    WriteI2C( 0x00 );
    IdleI2C();

    WriteI2C( Dec2BCD(second) );    // sec
    IdleI2C();
    WriteI2C( Dec2BCD(minute) );    // min
    IdleI2C();
    WriteI2C( Dec2BCD(hour) );    // hour
    IdleI2C();
    WriteI2C( Dec2BCD(dow+1) );     // dow
    IdleI2C();
    WriteI2C( 0x02 );
    IdleI2C();
    WriteI2C( 0x06 );
    IdleI2C();
    WriteI2C( 0x13 );
    IdleI2C();
    StopI2C();
    CloseI2C();    //close i2c if was operating earlier
}

void RTC_read()
{
    unsigned char x;
    CloseI2C();
    SSPADD=0x7F;
    SSPCON2bits.RCEN = 1;
    OpenI2C(MASTER,SLEW_ON);
    

    StartI2C();
    WriteI2C( 0xD0 );
    WriteI2C( 0x00 );
    StartI2C();
    WriteI2C( 0xD1 );
    x = ReadI2C();
    if(x&0x80) {
        powered_off = 1;
    }
    second = BCD2Dec(x&0x7F);
    AckI2C();
    minute = BCD2Dec(ReadI2C());
    AckI2C();
    hour = BCD2Dec(ReadI2C());
    AckI2C();
    dow = ReadI2C() -1;
    AckI2C();
    ReadI2C();
    AckI2C();
    ReadI2C();
    AckI2C();
    ReadI2C();
    AckI2C();
    ReadI2C();
    NotAckI2C();
    IdleI2C();

    StopI2C();
    CloseI2C();    //close i2c if was operating earlier


}





void CLK_DELAY()
{
    //unsigned char y; for(y=0;y<1;y++) {NOP(); NOP(); NOP(); NOP();}
}

void SetPWM(unsigned int pwm)
{
        pwm &= 0x0F;
        pwm <<= 1;
        pwm |= 0x940;
        CommandWriteHT1632C(pwm);
}
void clrscr()
{
        unsigned char chip;
        unsigned char addr;
        for(chip=1; chip<=CHIP_MAX;chip++) {
                for(addr=0; addr < 255; addr++) {
                        WriteHT1632C(chip, addr, 0);
                }
        }

}


void SetHT1632C_As3208(void)
{
        CommandWriteHT1632C(SYS_EN);
        CommandWriteHT1632C(LED_ON);
        CommandWriteHT1632C(RC_MASTER_MODE);
        CommandWriteHT1632C(N_MOS_COM8);
        CommandWriteHT1632C(PWM_10);
        CommandWriteHT1632C(BLK_OFF);
}

void OutputCLK_Pulse(void)      //Output a clock pulse
{
        CLK_74164 = 1;
        CLK_DELAY();
        CLK_74164 = 0;
        CLK_DELAY();
}

void OutputA_74164(unsigned char x)     //Input a digital level to 74164
{
        if(x==0) {
                A_74164 = 0; CLK_DELAY();
        } else {
                A_74164 = 1; CLK_DELAY();
        }
}

void CommandWriteHT1632C(unsigned int command) {

        unsigned char i;
        unsigned int j;

        command = command & 0x0fff;
        ChipSelect(0); CLK_DELAY(); ChipSelect(-1); CLK_DELAY();

        for(i=0; i<12; i++) {

                CLK = 0;
                CLK_DELAY();
                j = command & 0x0800;
                command = command << 1;
                j = j >> 11;
                DAT = j;
                CLK_DELAY();
                CLK = 1;
                CLK_DELAY();
        }
        ChipSelect(0);
}
unsigned char address;

void AddressWriteHT1632C(unsigned char myaddress)
{
        unsigned char i,temp;
        address = myaddress & 0x7f;
        CLK = 0;
        CLK_DELAY();
        DAT = 1;
        CLK_DELAY();
        CLK = 1;
        CLK_DELAY();
        CLK = 0;
        CLK_DELAY();
        DAT = 0;
        CLK_DELAY();
        CLK = 1;
        CLK_DELAY();
        CLK = 0;
        CLK_DELAY();
        DAT = 1;
        CLK_DELAY();
        CLK = 1;
        CLK_DELAY();
        for(i=0; i<7; i++) {
                CLK = 0;
                CLK_DELAY();
                temp = address & 0x40;
                address = address << 1;
                temp = temp >> 6;
                DAT = temp;
                CLK_DELAY();
                CLK = 1;
                CLK_DELAY();
        }
}
void WriteHT1632C(unsigned char chip, unsigned char myaddress, unsigned char data)
{
        unsigned char i,temp;
        address = myaddress & 0x3f;
        ChipSelect(0);
        ChipSelect(chip);
        CLK = 0;
        CLK_DELAY();
        DAT = 1;
        CLK_DELAY();
        CLK = 1;
        CLK_DELAY();
        CLK = 0;
        CLK_DELAY();
        DAT = 0;
        CLK_DELAY();
        CLK = 1;
        CLK_DELAY();
        CLK = 0;
        CLK_DELAY();
        DAT = 1;
        CLK_DELAY();
        CLK = 1;
        CLK_DELAY();
        for(i=0; i<7; i++) {
                CLK = 0;
                CLK_DELAY();
                temp = address & 0x40;
                address = address << 1;
                temp = temp >> 6;
                DAT = temp;
                CLK_DELAY();
                CLK = 1;
                CLK_DELAY();
        }
        for(i=0;i<8;i++) {
                CLK = 0;
                CLK_DELAY();
                temp = data & 0x01;
                DAT = temp;
                CLK_DELAY();
                CLK = 1;
                data = data >> 1;
                CLK_DELAY();
        }
//      ChipSelect(0);
}

void ChipSelect(int select)
{
        unsigned char tmp = 0;
        if(select<0) {
                OutputA_74164(0);
                CLK_DELAY();
                for(tmp=0; tmp<CHIP_MAX; tmp++) {
                        OutputCLK_Pulse();
                }
        } else if(select==0) {  //Disable all HT1632Cs
                OutputA_74164(1);
                CLK_DELAY();
                for(tmp=0; tmp<CHIP_MAX; tmp++) {
                        OutputCLK_Pulse();
                }
        } else {
                OutputA_74164(1);
                CLK_DELAY();
                for(tmp=0; tmp<CHIP_MAX; tmp++) {
                        OutputCLK_Pulse();
                }
                OutputA_74164(0);
                CLK_DELAY();
                OutputCLK_Pulse();
                CLK_DELAY();
                OutputA_74164(1);
                CLK_DELAY();
                tmp = 1;
                for( ; tmp<select; tmp++) {
                        OutputCLK_Pulse();
                }
        }
}

void draw_char(char x, char y, char c, unsigned char color)
{
        unsigned char chip;
        char addr = 0x00;
        int i,j;
        unsigned char mask, tmp;
        int base;
        unsigned char base_chip, resta;
        if (c>193) {
            return;
        }
        base = c;
        base = base << 3;
        addr = x*2;
        if (y==0) {
                base_chip = 1;
        } else if (y==1) {
                base_chip = 3;
        }
        //row = c * 8;
        // Transpose, val[0] = font[7] & 1, font[6] & 1, ...
        mask = 0x80;
        for(i=0;i<8;i++) {
                tmp = 0;
                j=7;
                while (j>=0) {
                        tmp = tmp << 1;
                        if (font[base+j] & mask) {
                                tmp = tmp | 0x01;
                        }

                        j--;
                }
                if (addr >=0 && addr < 32) {
                        resta = 0;
                        chip = base_chip;
                } else if (addr >= 32 && addr < 64) {
                        resta = 32;
                        chip = base_chip + 1;
                } else if (addr >= 64 && addr < 96) {
                        resta = 64;
                        chip = base_chip + 4;
                } else if (addr >= 96 && addr < 128) {
                        resta = 96;
                        chip = base_chip + 5;
                } else {
                        chip = 0;
                }

                if (color & 0x01) {
                        WriteHT1632C(chip, addr-resta, tmp);
                }
                if (color & 0x02) {
                        WriteHT1632C(chip, addr + 32 - resta, tmp);
                }

                addr += 2;
                mask = mask >> 1;
        }

}
void draw_row(char x, char y, char val, char color)
{
        unsigned char base_chip = 1, chip;
        unsigned char addr;
        unsigned char resta;

        if (y==0) {
                base_chip = 1;
        } else if (y==1) {
                base_chip = 3;
        }

        addr = x * 2;

                if (addr >=0 && addr < 32) {
                        resta = 0;
                        chip = base_chip;
                } else if (addr >= 32 && addr < 64) {
                        resta = 32;
                        chip = base_chip + 1;
                } else if (addr >= 64 && addr < 96) {
                        resta = 64;
                        chip = base_chip + 4;
                } else if (addr >= 96 && addr < 128) {
                        resta = 96;
                        chip = base_chip + 5;
                } else {
                        chip = 0;
                }

        if (color & 0x01) {
                WriteHT1632C(chip, addr-resta, val);
        }
        if (color & 0x02) {
                WriteHT1632C(chip, addr + 32 - resta, val);
        }


}

void init_hw(void)
{

    TRISC = 0b01011001;
    TRISB = 0b11111110;
    T0CON = 0b11000000;

    INTCON = 0b11100000;

    // Pull ups
    INTCON2 = 0;
    WPUB = 0xFF;
    LATB = 0;
    ANCON0 = 0;
    ANCON1 = 0;

    /// i2c
    SSPSTAT = 0x0;
    SSPCON1 = 0b00101000;


    

}

void delay_100ms()
{
    volatile long i,j;
    for(i=0; i<10000; i++) {
        for(j=0; j<1; j++);
    }
}

void delay_1s()
{
    volatile long i,j;
    for(i=0; i<10000; i++) {
        for(j=0; j<10; j++);
    }
}

//volatile char x,y,d,color;
unsigned char rot(unsigned char c)
{
    unsigned char r = 0;
    if (c&0x80) {
        r |= 0x01;
    }
    if (c&0x40) {
        r |= 0x02;
    }
    if (c&0x20) {
        r |= 0x04;
    }
    if (c&0x10) {
        r |= 0x08;
    }
    if (c&0x08) {
        r |= 0x10;
    }
    if (c&0x04) {
        r |= 0x20;
    }
    if (c&0x02) {
        r |= 0x40;
    }
    if (c&0x01) {
        r |= 0x80;
    }
    return r;
}

typedef unsigned int uint16_t;
typedef unsigned long uint32_t;
typedef unsigned short uint8_t;
#ifdef DCF77
int lowpass(int s) {

  const uint16_t decimal_offset = 10000;
  static uint32_t smoothed = 0;

  uint32_t input = s * decimal_offset;

  // compute N such that the smoothed signal will always reach 50% of
  // the input after at most 50 samples (=50ms).
  // N = 1 / (1- 2^-(1/50)) = 72.635907286
  uint16_t N = 72;
  smoothed = ((N-1) * smoothed + input) / N;

  // introduce some hysteresis
  static uint8_t square_wave_output = 0;

  if ((square_wave_output == 0) == (smoothed >= decimal_offset/2)) {
    // smoothed value more >= 50% away from output
    // ==> switch output
    square_wave_output = 1-square_wave_output;
    // ==> max the smoothed value in order to introduce some
    //     hysteresis, this also ensures that there is no
    //     "infinite memory"
    smoothed = square_wave_output? decimal_offset: 0;
  }
  return square_wave_output;
  //  return 0;
}
#endif
void draw_ball() {
    unsigned char ball_idx;
    unsigned char x;

    if (minute<15) {
        ball_idx = 0;
    } else if(minute<30) {
        ball_idx = 1;
    } else if(minute<45) {
        ball_idx = 2;
    } else {
        ball_idx = 3;
    }

    for(x=0; x<8; x++) {
        draw_row(22+x,1,rot(balls[ball_idx][x]),1);
        draw_row(22+x,1,0,2);
    }
    for(x=0;x<5; x++) {
        draw_row(17+x,1,0,3);
    }
    for(x=0;x<4; x++) {
        draw_row(30+x,1,0,3);
    }
    for(x=0;x<16; x++) {
        draw_row(17+x,1,0,2);
    }

}
void draw_normal_time()
{
    volatile int c;
    int ball_idx;
    int x;
    int hour_12;

    if (config_state != CONFIG_NORMAL) {
        return;
    }

    hour_12 = hour;
    if (hour>12) {
        hour_12 = hour-12;
    } else {
        hour_12 = hour;
    }
    if(hour_12 == 0) {
        hour_12 = 12;
    }


    if (hour_12>=10) {
        draw_char(19,0,130,1);  //1
    }

    // Obtenemos ultimo digito
    if (hour_12>=10) {
        hour_12 -=10;
    }

    draw_char(24,0,129+hour_12,1);

    

}
void clear_time_area() {
    int x;
    for(x=0; x<16; x++) {
        draw_row(17+x, 0,0,3);
        draw_row(17+x, 1,0,3);

    }
}
const char *dows[] = {"MON",
                "TUE",
                "WED",
                "THU",
                "FRI",
                "SAT",
                "SUN"};
void draw_str(unsigned char x, unsigned char y, unsigned char color, const char *str)
{
    const char *c= str;
    unsigned char base;
    while(*c) {
        if (*c>='A' && *c<='Z') {
            base = 143-'A';
        } else {
            base = 169-'a';
        }
        draw_char(x,y,base+*c,color);
        x+=6;
        c++;
    }
}
void draw_full_time()
{
    static int old_min=-1, old_hour=-1, old_dow=-1;
    unsigned char dec, uni;
    static unsigned char old_config = -1;

    if (old_config != config_state) {
        old_hour=-1;
        old_min = -1;
        old_dow = -1;
        old_config=config_state;
    }


    if (hour != old_hour) {
        clear_time_area();
        clrscr();
        old_hour = hour;
    }
    if (minute != old_min) {
        clear_time_area();
        old_min = minute;
    }
    if (dow != old_dow) {
        clear_time_area();
        old_dow = dow;
    }
    if (config_state == CONFIG_NORMAL) {
        return;
    }

    


    switch(config_state) {
        case CONFIG_HOUR:
            dec = hour % 10;
            uni = hour / 10;
            draw_char(17, 0, 129+uni,2);
            draw_char(24, 0, 129+dec,2);
            draw_str(0,1,1,"HOUR");
            break;
        case CONFIG_MIN:
            dec = minute % 10;
            uni = minute / 10;
            draw_char(17, 0, 129+uni,2);
            draw_char(24, 0, 129+dec,2);
            draw_str(0,1,1,"MIN");
            break;
        case CONFIG_DOW:
            draw_str(0,1,1,"DOW");
            draw_str(10,0,1,dows[dow]);
            break;
        case CONFIG_HOUR_BED:
            draw_str(0,1,1,"BED");
            dec = hour_bed % 10;
            uni = hour_bed / 10;
            draw_char(17, 0, 129+uni,2);
            draw_char(24, 0, 129+dec,2);
            break;
        case CONFIG_HOUR_WAKE:
            draw_str(0,1,1,"WAKE");
            dec = hour_wake % 10;
            uni = hour_wake / 10;
            draw_char(17, 0, 129+uni,2);
            draw_char(24, 0, 129+dec,2);
            break;
        case CONFIG_HOUR_BED_WE:
            draw_str(0,1,1,"BDWE");
            dec = hour_bed_we % 10;
            uni = hour_bed_we / 10;
            draw_char(17, 0, 129+uni,2);
            draw_char(24, 0, 129+dec,2);
            break;
        case CONFIG_HOUR_WAKE_WE:
            draw_str(0,1,1,"WKWE");
            dec = hour_wake_we % 10;
            uni = hour_wake_we / 10;
            draw_char(17, 0, 129+uni,2);
            draw_char(24, 0, 129+dec,2);
            break;

    }

}
void draw_time()
{
        draw_full_time();
        draw_normal_time();
}
void clear_bear()
{
    unsigned char x;
    for(x=0; x<=16; x++) {
        draw_row(x,0,0,3);
        draw_row(x,1,0,3);
    }
}
void draw_bear(int sleeping)
{
    int x;
    
    static unsigned char image_1 = 0;

    unsigned char c;
    if (config_state != CONFIG_NORMAL) {
        return;
    }
    // from sec 0 to sec 10 write text..
    if (second < 10) {
        if (minute%2) {
            draw_str(0,1,sleeping ? 2 : 1,"Marco");
        } else {
            if (sleeping) {
                if(powered_off) {
                    draw_str(0,1,2,"GGGG");
                } else {
                    draw_str(0,1,2,"ZZZZ");
                }
            } else {
                draw_str(0,1,1,"Hola");
            }
        }
        return;
    }
    
    c = second % 10;
    if (c<5) {
        image_1 = 0;
    } else {
        image_1 = 1;
    }
    if (sleeping) {
        for (x=0; x<sizeof(bear_0_slept)/sizeof(bear_0_slept[0]); x++) {
            if (image_1) {
                draw_row(x,0,rot(bear_0_slept[x]), 2);
            } else {
                draw_row(x,0,rot(bear_0a_slept[x]), 2);
            }
            draw_row(x,0,0, 1);
        }
        for (x=0; x<sizeof(bear_0_slept)/sizeof(bear_0_slept[0]); x++) {
            if (image_1) {
                draw_row(x,1,rot(bear_1_slept[x]), 2);
            } else {
                draw_row(x,1,rot(bear_1a_slept[x]), 2);
            }
            draw_row(x,1,0, 1);
        }
    } else {
        for (x=0; x<sizeof(bear_0_slept)/sizeof(bear_0_slept[0]); x++) {
            if (image_1) {
                draw_row(x,0,rot(bear_0_awake[x]), 1);
            } else {
                draw_row(x,0,rot(bear_0a_awake[x]), 1);
            }
            draw_row(x,0,0, 2);
        }
        for (x=0; x<sizeof(bear_0_slept)/sizeof(bear_0_slept[0]); x++) {
            if (image_1) {
                draw_row(x,1,rot(bear_1_awake[x]), 1);
            } else {
                draw_row(x,1,rot(bear_1a_awake[x]), 1);
            }
            draw_row(x,1,0, 2);
        }

    }
    draw_ball();

}



#define BCD_DEC(x)    ( (x & 0x0F) + 10*((x & 0xF0)>>4))
#ifdef DCF77
unsigned char SET_FIELD(char *data, int start, int w)
{
  unsigned char b = 0;
  int x;

  unsigned char mask=1;
  data += start;// point to the last one
  while(w--) {
    if (*data == 1) {
      b |= mask;

    }
    mask <<= 1;
    data++;
  }

  return b;

}
unsigned char CHECK_PAR(char *data, int start, int w)
{
  unsigned char rc = 0;
  data += start;
  while(w--) {
    if(*data == 1) {
      rc ^= 1;
    }
    data++;
  }
  return rc;
}

void fill_dcf(DCF77 *dcf, char *data)
{
  memset(dcf,0,sizeof(*dcf));
  dcf->cest = SET_FIELD(data,17,1);
  dcf->cet  = SET_FIELD(data,18,1);
  dcf->min  = SET_FIELD(data,21,7);
  dcf->hour = SET_FIELD(data,29,6);
  dcf->day =  SET_FIELD(data,36,6);
  dcf->year = SET_FIELD(data,50,8);
  dcf->dow  = SET_FIELD(data,42,3);
  dcf->P1   = SET_FIELD(data,28,1);
  dcf->P2   = SET_FIELD(data,35,1);
  dcf->P3   = SET_FIELD(data,58,1);
  dcf->calc_P1 = CHECK_PAR(data,21,7);
  dcf->calc_P2 = CHECK_PAR(data,29,6);
  dcf->calc_P3 = CHECK_PAR(data,36,22);
  if (dcf->P1 == dcf->calc_P1 &&
      dcf->P2 == dcf->calc_P2 &&
      dcf->P3 == dcf->calc_P3 &&
      dcf->cest != dcf->cet) {
    dcf->valid = 1;
  } else {
    dcf->valid = 0;
  }
}
void process_clock()
{
    DCF77 dcf;

    fill_dcf(&dcf,clock_buf);
    if (dcf.valid) {
        tRadio.Hour = BCD_DEC(dcf.hour);
        tRadio.Minute = BCD_DEC(dcf.min);
        tRadio.Second = 0;
        hour = tRadio.Hour;
        minute = tRadio.Minute;
        BEEP=1;
        delay_1s();
        BEEP=0;
    }

}
#endif

int main(int argc, char** argv)
{
    int last_min=-1;
    int last_hour = -1;
    powered_off = 0;
    //x=0;
    //y=0;
    //d=0x08;
    //color=2;
    t_idx = 0;
    clock_ready = 0;
    memset(t_data,255,sizeof(t_data));
    memset(clock_buf,255,sizeof(clock_buf));
    init_hw();
    //for(;;) {
//        Demo();
//    }
    BEEP = 1;
    delay_100ms();
    BEEP = 0;
    SetHT1632C_As3208();
    clrscr();
    /*
    hour = 0;
    minute=0;
    hour = eeprom_read(EEPROM_HOUR);
    minute = eeprom_read(EEPROM_MIN);
    dow = eeprom_read(EEPROM_DOW);
     * */
    hour_bed = eeprom_read(EEPROM_HOUR_BED);
    hour_wake = eeprom_read(EEPROM_HOUR_WAKE);
    hour_bed_we = eeprom_read(EEPROM_HOUR_BED_WE);
    hour_wake_we = eeprom_read(EEPROM_HOUR_WAKE_WE);

    RTC_read();
    second = 0;
    RTC_set();
    

    for( ;;) {
        BEEP = 0;
        //RTC_read();
        delay_100ms();
        BEEP = 0;
        delay_100ms();
        
        if (last_min!= minute || last_hour != hour) {
            clrscr();
            last_min = minute;
            last_hour = hour;
            //RTC_read();

        }
        
        if ((dow <5 && (hour>=hour_wake && hour<hour_bed)) ||
            (dow>=5 && (hour>=hour_wake_we && hour<hour_bed_we))){
            SetPWM(0x0F);
            draw_bear(0);
        } else {
            SetPWM(0x1);
            draw_bear(1);
        }
        draw_time();
#ifdef DCF77
        if (clock_ready) {
            BEEP=0;
            clock_ready = 0;
            process_clock();
        }
#endif
    }
    return (EXIT_SUCCESS);
}

//#define CNT_SEC  38997
#define CNT_SEC  39065
#define CNT_MS   39
#define REJ_TIME 70
#define REJECT_PULSE_WIDTH 50
#define PULSE_START     0
#define SYNC_TIME       1500
#define SPLIT_TIME      150
#define MAX_DEBOUNCE_REG    20
#define KB_REPEAT_FAST       500
#define KB_REPEAT_SLOW       5000

void interrupt ISR(void)
{
    static unsigned int auto_repeat = 0;
    static unsigned int KB_REPEAT = KB_REPEAT_SLOW;
    static long cnt = 0;
    static int cnt_ms = 0;
    static long msec=0;
    static long lastFlank=0;
    static char prev_level=2;
    int flankTime=0;
    static int i=0;
    char sensorValue;
    static unsigned long previousLeadingEdge=0;
    static unsigned long leadingEdge=0;
    static unsigned long trailingEdge=0;
    unsigned char Up = 0;
    int diff;

    if (INTCONbits.T0IF) {
        cnt++;
        cnt_ms++;


        if (cnt_ms>= CNT_MS) {
            cnt_ms = 0;
            msec++;
            BEEP = 0;

#ifdef DCF77
            // 10ms elapsed. Check radio
            //TOGGLE_DEBUG;
            sensorValue = lowpass(RADIO);
            

            if (prev_level != sensorValue) {
                flankTime = msec;
                prev_level = sensorValue;
                BEEP = 0;
                

                if ( (flankTime-previousLeadingEdge) < REJ_TIME ) {// Short pulse, ignore
                    goto exit_radio;
                }

                if ((flankTime-leadingEdge) < REJECT_PULSE_WIDTH) {
                    goto exit_radio;
                }

                
                if (sensorValue == 1) {
                    leadingEdge = flankTime;
                    
                } else {                    
                    // Flank Down
                    trailingEdge = flankTime;
                    diff = trailingEdge - leadingEdge;
                    if ((leadingEdge-previousLeadingEdge) > SYNC_TIME) {
                        // FINALIZE BUFFE
                        //BEEP = 1;
                        t_data[t_idx++] = 20;
                        //t_data_w[t_idx++] = diff;
                        t_idx = 0;
                        memcpy(clock_buf, t_data, sizeof(clock_buf));
                        clock_ready = 1;
                    }
                    previousLeadingEdge = leadingEdge;
                    if (diff < SPLIT_TIME) {
                        // ADD 0
                        t_data[t_idx++] = 0;
                        //LATCbits.LATC7 = 0;
                        //t_data_w[t_idx++] = diff;
                    } else {
                        // ADD 1
                        t_data[t_idx++] = 1;
                        //LATCbits.LATC7 = 1;
                        //t_data_w[t_idx++] = diff;

                }
                    

            }
                

        }

#endif
        // scan kbd
            if (PB0==0 && debounce_reg[0] < MAX_DEBOUNCE_REG) {
                    debounce_reg[0]++;
            } else if ( PB0 == 1 && debounce_reg[0]>0) {
                    debounce_reg[0]--;
            }

            if (PB1==0 && debounce_reg[1] < MAX_DEBOUNCE_REG) {
                    debounce_reg[1]++;
            } else if ( PB1 == 1 && debounce_reg[1]>0) {
                    debounce_reg[1]--;
            }


            if (last_button_state[0] == 0 && debounce_reg[0] >= MAX_DEBOUNCE_REG) { // button pressed
                last_button_state[0] = 1;
                // button pressed..
                BEEP = 1;
                config_state++;
                if (config_state>=CONFIG_MAX) {
                    second = 0;
                    eeprom_write(EEPROM_HOUR,hour);
                    eeprom_write(EEPROM_MIN,minute);
                    eeprom_write(EEPROM_DOW,dow);
                    eeprom_write(EEPROM_HOUR_BED,hour_bed);
                    eeprom_write(EEPROM_HOUR_WAKE,hour_wake);
                    eeprom_write(EEPROM_HOUR_BED_WE,hour_bed_we);
                    eeprom_write(EEPROM_HOUR_WAKE_WE,hour_wake_we);
                    RTC_set();
                    powered_off = 0;

                    config_state = 0;
                }
                delay_100ms();
                BEEP = 0;
            } else if (last_button_state[0] == 1 && debounce_reg[0] == 0) { // button released
                last_button_state[0] = 0;
            }

        }
        // Auto repeat
        if (debounce_reg[1] >= MAX_DEBOUNCE_REG && ( auto_repeat++>KB_REPEAT || last_button_state[1] == 0)) { // button pressed
            last_button_state[1] = 1;

            if (KB_REPEAT >  KB_REPEAT_FAST) {
                KB_REPEAT-=50;
            }
            auto_repeat = 0;
            // button pressed..
            switch(config_state) {
                case CONFIG_HOUR:
                    BEEP = 1;

                    hour++;
                    if (hour>23) {
                        hour=0;
                    }
                    break;
                case CONFIG_MIN:
                    BEEP = 1;
                    minute++;
                    if (minute>59) {
                        minute=0;
                    }
                    break;
                case CONFIG_DOW:
                    BEEP = 1;
                    dow++;
                    if (dow>6) {
                        dow=0;
                    }
                    break;
                case CONFIG_HOUR_WAKE:
                    BEEP = 1;

                    hour_wake++;
                    if (hour_wake>23) {
                        hour_wake=0;
                    }
                    break;
                case CONFIG_HOUR_BED:
                    BEEP = 1;

                    hour_bed++;
                    if (hour_bed>23) {
                        hour_bed=0;
                    }
                    break;
                case CONFIG_HOUR_WAKE_WE:
                    BEEP = 1;

                    hour_wake_we++;
                    if (hour_wake_we>23) {
                        hour_wake_we=0;
                    }
                    break;
                case CONFIG_HOUR_BED_WE:
                    BEEP = 1;

                    hour_bed_we++;
                    if (hour_bed_we>23) {
                        hour_bed_we=0;
                    }
                    break;


            }

        } else if (last_button_state[1] == 1 && debounce_reg[1] == 0) { // button released
            last_button_state[1] = 0;
            KB_REPEAT = KB_REPEAT_SLOW;
        }

        

        exit_radio:
        
        if (cnt>=CNT_SEC) {
            cnt = 0;

            second++;
            if (second>=60) {
                minute++;
                second = 0;
            }
            if (minute>=60) {
                hour++;
                minute = 0;
            }
            if (hour>=24) {
                hour=0;
                dow++;
            }
            if (dow>=7) {
                dow=0;
            }
        }

        INTCONbits.T0IF = 0;
    }

}